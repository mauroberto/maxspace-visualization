var instance = undefined;
var solutions = [];
var solutionsCount = 0;
var container, nav, floatButton, popups;
var instanceSelector, solutionSelector, solutionSelectorFloatButton;
var alignTop = false;
const MAXSPACE = 1;
const MAXSPACERDWV = 2;
const MAXSPACEP = 3;

const SLOT_SIZE = 260;

const itemTemplate = [
    '<div onclick="openPopup({{item.id}})" id="item{{item.id}}-slot{{slot.id}}-solution{{solution.id}}" class="item item{{item.id}}" style="height:{{percentSize}}%;background-color:{{item.color}}">',
        '<span>A<sub>{{item.id}}</sub></span>',
    '</div>',
].join('');

const slotTemplate = [
    '<div class="slot {{#alignTop}}align-top{{/alignTop}}" id="slot{{slot.id}}-solution{{solution.id}}">',
        '<span class="slot-title">Slot<sub>{{slot.id}}</sub></span>',
        '<div class="slot-fullness" style="{{#alignTop}}top:calc({{percentFullness}}% + 2px);{{/alignTop}}{{^alignTop}}bottom:{{percentFullness}}%;{{/alignTop}}">',
            '<span>{{slot.fullness}}</span>',
        '</div>',
    '</div>'
].join('');

const solutionTemplate = [
    '<div class="solution" id="solution{{solution.id}}">',
        '<div>',
            '<h2 class="solution-title">{{solution.name}} - obj: {{solution.obj}}</h2>',
            '<a title="Fechar" class="solution-remove" onclick="removeSolution({{solution.id}});"></a>',
            '<div class="solution-size"><span>{{instance.L}}</span></div>',
        '</div>',
        '<div class="solution-container" id="solution{{solution.id}}container"></div>',
    '</div>'
].join('');

var popupTemplate = [
    '<div class="popup" id="popup{{id}}">',
        '<a title="Fechar" class="popup-remove" onclick="closePopup({{id}})"></a>',
        '<div class="color" style="background-color:{{color}}"><span>A<sub>{{id}}</sub></span></div>',
        '<div class="info">',
            '<div>e<sub>i</sub>: <span>{{e}}</span></div>',
            '<div>s<sub>i</sub>: <span>{{s}}</span></div>',
            '<div>w<sub>i</sub><sup>min</sup>: <span>{{w}}</span></div>',
            '<div>w<sub>i</sub><sup>max</sup>: <span>{{wmax}}</span></div>',
            '<div>r<sub>i</sub>: <span>{{r}}</span></div>',
            '<div>d<sub>i</sub>: <span>{{d}}</span></div>',
            '<div>v<sub>i</sub>: <span>{{v}}</span></div>',
        '</div>',
    '</div>'
].join('');

var removeSolution = function(solutionId){
    const solutionContainer = document.getElementById("solution"+solutionId);
    solutionContainer.remove();
}

var openPopup = function(id) {
    var popup = document.getElementById("popup"+id);
    var popups = document.getElementsByClassName("popup");

    for (var i = 0; i < popups.length; i++) {
        if (popups[i] == popup) continue;
        popups[i].classList.remove("show");
    }
    popup.classList.add("show");
}

var closePopup = function(id) {
    var popup = document.getElementById("popup"+id);
    popup.classList.remove("show");
}

var renderPopup = function(item) {
    var output = Mustache.render(popupTemplate, item);

    popups.insertAdjacentHTML('beforeend', output);
}

var renderItem = function(item, slot, solution){
    const slotContainer = document.getElementById("slot"+slot.id+"-solution"+solution.id);

    var percentSize = (item.s*100.0)/instance.L;


    var output = Mustache.render(itemTemplate, {item, percentSize: percentSize});


    slotContainer.insertAdjacentHTML('afterbegin', output);
};


var renderSlot = function(slot, solution){
    const solutionContainer = document.getElementById("solution"+solution.id+"container");

    var percentFullness = (slot.fullness*100.0)/instance.L;

    var output = Mustache.render(slotTemplate, {solution, slot, alignTop: alignTop, percentFullness: percentFullness});

    solutionContainer.insertAdjacentHTML('beforeend', output);

    if (alignTop) {
        for (var i = slot.items.length-1; i >= 0; i--){
            var item = instance.items[slot.items[i]];
            renderItem(item, slot, solution);
        }
    } else {
        for (var i = 0; i < slot.items.length; i++){
            var item = instance.items[slot.items[i]];
            renderItem(item, slot, solution);
        }
    }
};


var renderSolution = function(container, solution){

    var output = Mustache.render(solutionTemplate, {solution, instance});

    container.insertAdjacentHTML('beforeend', output);

    for(var i=0; i < instance.K; i++){
        var slot = {};
        slot.id = i;
        slot.L = instance.L;
        slot.items = solution.slots[i];
        slot.fullness = 0.0;
        for (var j = 0; j < slot.items.length; j++){
            slot.fullness += instance.items[slot.items[j]].s;
        }
        renderSlot(slot, solution);
    }
};


var testarSolucao = function(instance, solution){

    if (solution.slots.length != instance.K){
        console.error("Solução inválida. K != solution.slots.length.");
        console.log(instance.K, " ", solution.slots.length);
        return false;
    }

    var cont = [];

    for (var i = 0; i < instance.A; i++){
        cont[i] = 0;
    }

    for (var j = 0; j < solution.slots.length; j++){
        for (var i = 0; i < solution.slots[j].length; i++){
            cont[solution.slots[j][i]]++;
        }  
    }

    for (var i = 0; i < instance.A; i++){
        if (cont[i] != 0 && cont[i] != instance.items[i].w){
            console.error("Solução inválida. Frequência do item ", i, " inválida.");
            console.log(cont[i], " ", instance.items[i].w);
            return false;
        }
    }

    return true;
};

var readInstance = function(textInstance){
    var lines = textInstance.split(/\r\n|\n/);
    instance = {};

    var line = lines[0].split(" ");
    instance.A = parseInt(line[0]);
    instance.K = parseInt(line[1]);
    instance.L = parseInt(line[2]);
    instance.items = [];

    for (var i = 1; i < lines.length; i++){
        var trim = lines[i].trim();
        if (!trim.length) continue;

        line = trim.split(" ");
        var item = {};
        item.id = parseInt(line[0]);
        item.s = parseInt(line[1]);
        item.r = parseInt(line[2]);
        item.d = parseInt(line[3]);
        item.w = parseInt(line[4]);
        item.wmax = parseInt(line[5]);
        item.v = parseFloat(line[6]);
        item.color = randomColor({luminosity: 'light'});
        item.e = (item.v/item.s).toFixed(3);
        instance.items.push(item);
    }

    popups.innerHTML = "";
    for (var i = 0; i < instance.A; i++) {
        renderPopup(instance.items[i]);
    }

    
    console.log("instance", instance);
};


var readSolution = function(textInstance, name){
    var lines = textInstance.split(/\r\n|\n/);
    var solution = {};
    solution.id = this.solutionsCount++;
    solution.name = name;
    solution.slots = [];
    solution.obj = parseFloat(lines[0]);

    for (var i = 1; i < lines.length; i++){
        var trim = lines[i].trim();
        if (!trim.length) continue;

        var line = trim.split(" ");
        solution.slots.push([]);

        for (var j = 1; j < line.length; j++){
            solution.slots[i-1].push(parseInt(line[j]));
        }
    }

    solutions.push(solution);

    console.log("solution", solution);

    return solution;
};

var runStub = function(container){

    instance = {};
    instance.K = 2;
    instance.L = 100;
    instance.A = 4;

    instance.items = [
        {
            id: 0,
            s: 10.0,
            v: 5.0,
            w: 2,
            color: randomColor({luminosity: 'light'})
        },
        {
            id: 1,
            s: 15.0,
            v: 9.0,
            w: 1,
            color: randomColor({luminosity: 'light'})
        },
        {
            id: 2,
            s: 12.0,
            v: 24.0,
            w: 2,
            color: randomColor({luminosity: 'light'})
        },
        {
            id: 3,
            s: 63.0,
            v: 24.0,
            w: 1,
            color: randomColor({luminosity: 'light'})
        }
    ];

    var solution = {
        name: "Solucao 0",
        id: 0,
        slots: [
            [
                0, 1, 2, 3
            ],
            [
                0, 2
            ]
        ]
    };

    var solution2 = {
        name: "Solucao 1",
        id: 1,
        slots: [
            [
                0, 2, 3
            ],
            [
                0, 1, 2
            ]
        ]
    };

    if(testarSolucao(this.instance, solution)){
        renderSolution(container, solution);
    }

    if(testarSolucao(this.instance, solution2)){
        renderSolution(container, solution2);
    }

}

var changeInstanceSelector = function(event){
    if (event.target.files.length == 0) return;

    const file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = function (evt) {
        readInstance(evt.target.result);
    }
    reader.onerror = function (evt) {
        console.log("error reading file");
    }
};

var changeSolutionSelector = function(event){
    for (var i = 0; i < event.target.files.length; i++){
        const file = event.target.files[i];
        var reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = function (evt) {
            var name = file.name;
            var solution = readSolution(evt.target.result, name);
            if(testarSolucao(instance, solution)){
                renderSolution(container, solution);
            }
        }
        reader.onerror = function (evt) {
            console.log("error reading file");
        }
    }
};

var changeProblemSelector = function(event){
    if (event.target.value == MAXSPACEP) {
        alignTop = true;
    } else if (event.target.value == MAXSPACE || event.target.value == MAXSPACERDWV) {
        alignTop = false;
    }
};

var addSolutionButtonClick = function(){
    solutionSelector.value = null;
}


window.onload = function(){

    container = this.document.getElementById("mainContainer");
    nav = this.document.getElementById("nav");
    floatButton = this.document.getElementById("solution-selector-float-button");
    popups = document.getElementById("popups");

    container.style.backgroundColor = randomColor({luminosity: 'light', hue: 'purple'});
    nav.style.backgroundColor = floatButton.style.backgroundColor = randomColor({luminosity: 'dark', hue: 'purple'});

    instanceSelector = document.getElementById("instance-selector");
    solutionSelector = document.getElementById("solution-selector");
    problemSelector = document.getElementById("problem-selector");
    solutionSelectorFloatButton = document.getElementById("solution-selector-float-button");


    instanceSelector.addEventListener("change", changeInstanceSelector);
    solutionSelector.addEventListener("change", changeSolutionSelector);
    problemSelector.addEventListener("change", changeProblemSelector);

    solutionSelectorFloatButton.addEventListener("click", addSolutionButtonClick);
};
